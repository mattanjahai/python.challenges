graphic = """
      ____
     /___/\_                                
    _\   \/_/\__                         
  __\       \/_/\                    
  \   __    __ \ \                    
 __\  \_\   \_\ \ \   __              
/_/\\\   __   __  \ \_/_/\             
\_\/_\__\/\__\/\__\/_\_\/          
   \_\/_/\       /_\_\/              
      \_\/       \_\/

    WELCOME TO THE GAME""" 

win_game = """
                                                                                                                                                                               
  mm@***@m@  mm@**@@m  *@@@m   *@@@*  mm@***@m@ *@@@***@@m        @@      @@@**@@**@@@*@@@*   *@@@**@@@@*          @@      @@@**@@**@@@*@@@@*  mm@**@@m  *@@@m   *@@@* m@***@m@
m@@*     *@m@@*    *@@m  @@@m    @  m@@*     *@   @@   *@@m      m@@m     @*   @@   *@ @@       @    @@           m@@m     @*   @@   *@  @@  m@@*    *@@m  @@@m    @  m@@    *@
@@*       *@@*      *@@  @ @@@   @  @@*       *   @@   m@@      m@*@@!         @@      @@       @    @@          m@*@@!         @@       @@  @@*      *@@  @ @@@   @  *@@@m    
@@         @@        @@  @  *@@m @  @!            !@@@@@@      m@  *@@         !@      @@       @    @@         m@  *@@         !@       @@  @@        @@  @  *@@m @    *@@@@@m
@!m        @@        @@  @   *@@m!  @!m    *@@@@  !@  @@m      @@@!@!@@        !@      @@       !    @!     m   @@@!@!@@        !@       @!  @@        @@  @   *@@m!        *@@
*!@m     m**@@      @@*  !     !@!  *!@m     @@   !@   *!@    !*      @@       !@      @@       !    @!    :@  !*      @@       !@       @!  *@@      @@*  !     !@!  @@     @@
!!!        !@@      !@!  !   *!!!!  !!!    *!@!!  !@  ! !!     !!!!@!!@        !@      !@       !    !!     !   !!!!@!!@        !@       !!  !@@      !@!  !   *!!!!  !     *@!
:!!:     !**@!!!    !!!  !     !!!  *:!!     !!   !!   *!!!   !*      !!       !!      !!!     !!    !:    !!  !*      !!       !!       :!  *@!!!    !!!  !     !!!  !!     !!
  : : : :!   : : : :   : : :    :!!   ::: : ::  : :!:  : : :: : :   : :::    : :!::     : : : :!:  : :: !: : : : :   : :::    : :!::   :!: :   : : : :   : : :    :!! :!: : :!                                                                                                                                                                                                                      Congratulations! You've found the lost treasure; now you just have to figure out how to bring it home with you! I hope you enjoyed the adventure!

                Congratulations! You've found the lost treasure; now you just have to figure out how to bring it home with you! I hope you enjoyed the adventure!                                                                                                              
"""










class Room:
    def __init__(self, current_room):
        self.current_room = current_room

       
class Forest(Room):
    description = f"""You are on the path winds through a dense, green forest, surrounded by ancient trees whose vast canopies create a green roof over your head.
    Sunlight filters through the leaves, casting a dance of bright spots on the ground. The rustle of leaves and the sounds of nature accompany every step.
Soft, slightly damp earth and fallen leaves underfoot. The scent of freshness and earth mixes with the delicate aroma of moss.
The path leads to a mysterious cave, looming in the distance, arousing curiosity and slight anxiety. It is a place full of secrets and nature, where time seems to stand still."""

    items = []
    
class Cave_Entrance(Room):
    description = f"""You come across old, massive doors at the foot of a mountain. They appear worn by time, with faded wood and rusty hinges.
Ivy and moss entwine them, evidencing years of neglect. The air around is cool and damp. On closer inspection, you notice indecipherable inscriptions, adding a mysterious charm.
The place's atmosphere is unsettling yet intriguing, as if these doors guard secrets deep within the mountain cave."""

    items = ["rock", "stick", "key"]
    

class Inside_Cave(Room):
    description = f"""The cave's interior is spacious, with raw, rocky walls echoing the soft drips of water in the distance.
The air is damp, and faint rays of light seeping through small crevices in the rock create irregular patterns on the floor.
In one corner, partially hidden in the shadows, stands an old chest. Its wooden panels are weathered, and the metal fittings are covered in rust.
Around it grows several patches of green moss, adding a mysterious charm to the place. The atmosphere is enigmatic, as if the cave holds more secrets than just the chest."""

    items = ["chest"]

rock = """This stone, smooth and cool to the touch, has multicolored veins and a slight shimmer, fitting perfectly in the palm."""
stick = """The stick you see appears light and sturdy, with an irregular texture and a few dry leaves. It seems like a good option for prying something open. """
key = """Here is a rusted key, weathered by the ravages of time. It seems to fit a small lock."""
chest = """You see an old chest, moss-covered and seemingly ordinary, massive and cumbersome. It surely conceals unknown treasures hidden beneath its antiquated, wooden lid."""
treasure_map = """This treasure map, faded and yellowed with age, features mysterious symbols and paths leading to hidden riches."""


with open('descriptions.txt', 'r', encoding='utf-8') as file1:
    start = file1.read()
    


class Player:

    current_room = Forest
    inventory = ["treasure_map"]
    cave_door_open = False
    game_on = True 

    def show():
        if len(Player.inventory) == 0:
            print("You have nothing with you.")
        else:
            print (f"Your inventory: {Player.inventory}")

    def take_item():
        item = input("What you want to take? : ")
        
        if item not in Player.current_room.items:
            print("Type one more time, there is nothing like that here!")
        else:
            Player.inventory.append(item)
            Player.current_room.items.remove(item)

    def describe(a):        
        if a == "rock":
            if "rock" in Player.inventory:
                print(rock)
            else:
                print("You don't have this item")
        elif a == "key":
            if "key" in Player.inventory:
                print(key)
            else:
                print("You don't have this item")            
        elif a == "stick":
            if "stick" in Player.inventory:
                print(stick)
            else:
                print("You don't have this item")
        elif a == "chest":
                if Player.current_room == Inside_Cave:
                    print(chest)
                else:
                    print("You must be near the chest to see it's description")
        elif a == "treasure_map":
                if "stick" in Player.inventory:
                    print(treasure_map)
                else:
                    print("You dont have this thing in your inventory!")
        else:
            print("Wrong typing! Try one more time!")

    def look():
        print(Player.current_room.description)
        if len(Player.current_room.items) > 0:
            print(f"You find some things like:")
            print(Player.current_room.items)
        else:
            print("There is nothing more you can see in here.") 

    def move(a):
        if a == "Forest":
            print(f"You are on wild nature, like forest.")
            Player.current_room = Forest
            return Player.current_room
            
        elif a == "Cave_Entrance":
            print("You are in front of the door to the cave ")
            Player.current_room = Cave_Entrance
            return Player.current_room

        elif a == "Inside_Cave" and Player.cave_door_open == False:
            print("You cant enter the cave until the door will be open.")
            
        elif a == "Inside_Cave" and Player.cave_door_open == True:
            print("You are now in a cave.")
            Player.current_room = Inside_Cave
            return Player.current_room
        
        else:
            print("There is no place like that! Maybe wrong typing? Try one more time!")


    def use_item():
        item = input("What thing you want to use? : ").lower()
        
        if item == "rock":
            if "rock" not in Player.inventory:
                print("You dont have that in your inventory!")        
            elif Player.current_room == Forest:
                print("You dont know on what you can use rock in here, so you put it in your pocket agan.")
            elif Player.current_room == Cave_Entrance:
                print("You threw your stone at the door, but nothing happened. The stove made a dull thud.")
            elif Player.current_room == Inside_Cave:
                print("You see that there is a chance that the rock can be more usefull. You see a chest, and lock - maybe there is a connection?")
            else:
                print("You have to use your rock some elsewhere.")
                
        elif item == "stick":
            if "stick" not in Player.inventory:
                print("You dont have anything like that in your inventory!")
            elif Player.current_room == Forest:
                print("You used the stick on the ground, but after a few minutes you realize that it is not effective for anything.")
            elif Player.current_room == Cave_Entrance:
                print("You used a stick, you tried to break the door, but there's not enough power to do so, I'm sorry.")
            elif Player.current_room == Inside_Cave:
                print("You see that there is a chance that stick can be more usefull. You see a chest, and lock - maybe there is a connection?")
            else:
                print("You have to use your stick elsewhere.")

        elif item == "key":
            if "key" not in Player.inventory:
                print("You dont have anything like that in your inventory!")
            elif Player.current_room == Forest:
                print(f"""You dont know what to do with this rusty hey in the forest, so you put it in your pocket agan.""")                
            elif Player.current_room == Cave_Entrance:
                print("Eureka! You used this old, rusty key on the door lock. Shh khhh phh.. And the doors are open!")
                Player.cave_door_open = True
            elif Player.current_room == Inside_Cave:
                print("You have the key, but is there any lock that it can fit more?Let's check!")
            else:
                print("You have to use the Key elsewhere.")

        elif item == "chest":
            if Player.current_room == Forest:
                print("You cant see anything like chest in here.")
            elif Player.current_room == Cave_Entrance:
                print("You cant see anything like chest in here. Maybe inside?")
            elif Player.current_room == Inside_Cave:
                Player.chest_lock()
            else:
                print("You cant see anything like chest in here as well, try somewhere else.")
                
        elif item == "treasure_map":
            print(f"""You cant use it on more things than read it's description. Try something else.""")
        else:
            print("Try using diffrent item.")
            

    def chest_lock():
        print("You came closer and saw the padlock, which was already quite rusty. Maybe it will be possible to destroy it or open it somehow.")
        action = input("What kind of your items you want to use?: ").lower()
        if action == "key":
            print(f"""Haha! Nice try, but this rusty key is too small and dont fit the padlock. Try another one!""")
        elif action == "stick":
            print(f"""Oh no! You tried to pry the lock, you almost succeeded. Unfortunately, the stick broke.""")
        elif action == "rock":
            print(f"""One, two and three! Not bad, a few hard hits and I managed to break the lock.!""")
            print(win_game)
            Player.game_on = False
        else:
            print("Sorry, that's possible. Try one more time.")
            
    def help():
        print(f"""Game Commands:
                help             - show help message
                move [direction] - move to next action game location 
                                  ('Forest' or 'Cave_Entrance' or 'Inside_Cave')
                take [item]      - take an item from the current room
                describe [item]  - description of an item and it's possibile use
                use  [item]      - use item 
                inventory        - show your inventory
                look             - look around the current room""")





print(graphic)
start_game = input("\nType 'ready' to begin! - ")
if start_game == "ready":
    print(start)

    while Player.game_on == True:
        action = input("\nWhat is your action? - ").lower()
        
        if action == "show":
            Player.show()

        elif action == "move":
            print("Move where?")
            Player.move(input(" < "))

        elif action == "describe":
            Player.describe(input("Enter item: ").lower())                  
                         
        elif action == "take":
            Player.take_item()
            
        elif action == "use":
            Player.use_item()
                
        elif action == "look":
            Player.look()

        elif action == "help":
            Player.help()

        elif action == "chest" and Player.current_room == Inside_Cave:
            Player.chest_lock()

        else:
            print("Thanks but there is no action like that, let's try one more time.")
 
    
    
