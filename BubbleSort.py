lista = [64, 34, 25, 12, 22, 11, 90]


def bubble_sort(r):
    n = len(r)
    for i in range(n):
        
        swapped = False
        for j in range(0, n - i - 1):
            if r[j] > r[j + 1]:
                r[j], r[j + 1] = r[j + 1], r[j]
                swapped = True
        if not swapped:
            break
    return r


