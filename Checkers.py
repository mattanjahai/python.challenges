class CheckersGame:

    def __init__(self):
        self.board = self.create_board()
        self.player_one_name = "Player 1 - [O]"
        self.player_two_name = "Player 2 - [X]"
        self.current_player = self.player_one_name
        
        
    def checkers_conditions(self):
        description = "The basic rules of the game Checkers are:\n1. Playing on the empty squares of a checkers board\n2. 'O' pawns start the game\n3. The goal is to capture all of your opponent's pieces\n4. Pieces move forward only one square in the middle capture occurs when an opponent's pawn accidentally jumps over an empty square\n5. After the pawn reaches the last row on the checkers board, the opponent's side becomes king\n6. King has the ability to move around the main areas on the other side the game ends in a draw when players cannot capture their opponent's piece. \n7. When specifying the move, please select your pawn by specifying the row and column digitally, then enter the target field in a similar way.\n8.'[_]' - means empty space when you can put your piece.\n9. r/c - means r for rows, c for columns. "
        print(description)
         
    
    def create_board(self):
        self.board =  [
        ["r\c"," 1 "," 2 "," 3 "," 4 "," 5 "," 6 "," 7 "," 8 "],  
        [" 1 ","[_]","[■]","[_]","[■]","[_]","[■]","[_]","[■]"], 
        [" 2 ","[■]","[_]","[■]","[_]","[■]","[_]","[■]","[_]"],
        [" 3 ","[_]","[■]","[_]","[■]","[_]","[■]","[_]","[■]"], 
        [" 4 ","[■]","[_]","[■]","[_]","[■]","[_]","[■]","[_]"],
        [" 5 ","[_]","[■]","[_]","[■]","[_]","[■]","[_]","[■]"], 
        [" 6 ","[■]","[_]","[■]","[_]","[■]","[_]","[■]","[_]"],
        [" 7 ","[_]","[■]","[_]","[■]","[_]","[■]","[_]","[■]"], 
        [" 8 ","[■]","[_]","[■]","[_]","[■]","[_]","[■]","[_]"]
    ]
        return self.board        

    def create_pieces(self):
        for i in range(9):
            if self.board[1][i] == "[_]":
                self.board[1][i] = self.board[1][i].replace("[_]", "[O]")
            if self.board[2][i] == "[_]":
                self.board[2][i] = self.board[2][i].replace("[_]", "[O]")
            if self.board[3][i] == "[_]":
                self.board[3][i] = self.board[3][i].replace("[_]", "[O]")
            if self.board[6][i] == "[_]":
                self.board[6][i] = self.board[6][i].replace("[_]", "[X]")
            if self.board[7][i] == "[_]":
                self.board[7][i] = self.board[7][i].replace("[_]", "[X]")
            if self.board[8][i] == "[_]":
                self.board[8][i] = self.board[8][i].replace("[_]", "[X]")

    def refresh_board(self):
        print(" ")
        for row in self.board:
            row_string = ""  
            for cell in row:
                row_string = row_string + cell + " "  
            print(row_string.rstrip())      
        print(" ")  
        print(f"Current player move : {self.current_player}")
        
    def taking_move(self):
        while True:
            try:
                start_input = input("Please, enter the start row and column separated by a space (like: '1 3'): ")
                start_row, start_col = [int(val) for val in start_input.split()]
                end_input = input("Please, enter the end row and column separated by a space (like: '2 4'): ")
                end_row, end_col = [int(val) for val in end_input.split()]

                #Check if the selected piece belongs to the current player
                if not self.is_correct_players_piece(start_row, start_col):
                    raise ValueError("The selected piece does not belong to you.")

                #Check the move is within bounds
                if not (1 <= start_row <= 9 and 1 <= start_col <= 9 and 1 <= end_row <= 9 and 1 <= end_col <= 9):
                    raise ValueError("Move is out of bounds.")
                break

            except ValueError:
                print(f"Invalid input. Try again.")
                
        return start_row, start_col, end_row, end_col


    def is_correct_players_piece(self, start_row, start_col):
        piece = self.board[start_row][start_col]
        if self.current_player == self.player_one_name and piece in ["[O]", "[KO]"]:
            return True
        elif self.current_player == self.player_two_name and piece in ["[X]", "[KX]"]:
            return True
        return False

    def switch_player(self):
        if self.current_player == self.player_one_name:
            self.current_player = self.player_two_name
        else:
            self.current_player = self.player_one_name

    def promote_to_king(self, row, col):
        if self.board[row][col] == "[X]" and row == 1:   
            self.board[row][col] = "[KX]"
        elif self.board[row][col] == "[O]" and row == 8:  
            self.board[row][col] = "[KO]"

    def check_winner(self):
        player_one_pieces = sum((row.count("[O]") + row.count("[KO]")) for row in self.board)
        player_two_pieces = sum((row.count("[X]") + row.count("[KX]")) for row in self.board)

        if player_one_pieces == 0:
            print(f"{self.player_two_name} wins!")
            return self.player_two_name
        elif player_two_pieces == 0:
            print(f"{self.player_one_name} wins!")
            return self.player_one_name
        return None

    def is_opponent_piece(self, row, col):
        piece = self.board[row][col]
        if self.current_player == self.player_one_name and piece in ["[X]", "[KX]"]:
            return True
        elif self.current_player == self.player_two_name and piece in ["[O]", "[KO]"]:
            return True
        return False

    def find_possible_captures(self, start_row, start_col):
        captures = []
        directions = [(-1, -1), (-1, 1), (1, -1), (1, 1)]  
        for drow, dcol in directions:
            opponent_row, opponent_col = start_row + drow, start_col + dcol
            landing_row, landing_col = opponent_row + drow, opponent_col + dcol
            # heck if the landing position is within the board
            if 1 <= landing_row <= 8 and 1 <= landing_col <= 8:
                #Check if the next square has an opponent piece and the square after is empty
                if self.is_opponent_piece(opponent_row, opponent_col) and self.board[landing_row][landing_col] == "[_]":
                    captures.append(((opponent_row, opponent_col), (landing_row, landing_col)))
        return captures

    
    def is_move_valid(self, start_row, start_col, end_row, end_col, player_piece):
        #Check if the move is within the board's boundaries
        if not (1 <= start_row <= 8 and 1 <= start_col <= 8 and 1 <= end_row <= 8 and 1 <= end_col <= 8):
            print("Move beyond borders, try one more time.")
            return False
        
        start_piece = self.board[start_row][start_col]
        end_piece = self.board[end_row][end_col]
        
        #Ensure the destination is empty
        if end_piece != "[_]":
            print("Destination is not empty.")
            return False

        #Check if the choosed piece is correct for player.
        piece = self.board[start_row][start_col]
        if self.current_player == self.player_one_name and piece in ["[O]", "[KO]"]:
            return True
        elif self.current_player == self.player_two_name and piece in ["[X]", "[KX]"]:
            return True
        else:
            print("No valid piece selected.")
            return False

        #To calculate movement direction and distance
        row_diff = end_row - start_row
        col_diff = end_col - start_col

        #IF statment to check for diagonal movement of more than one square without capture
        if abs(row_diff) > 1 and abs(col_diff) > 1:
            capture_possible = False
            
            #Calculate intermediate square for potential capture
            mid_row = start_row + (row_diff // abs(row_diff))
            mid_col = start_col + (col_diff // abs(col_diff))
            
            #Check if an opponent's piece is in the intermediate square
            if self.is_opponent_piece(mid_row, mid_col):
                capture_possible = True
            
            #If moving more than one square diagonally without capturing, move is invalid
            if not capture_possible:
                print("Invalid move. You can only move one more square diagonally if capturing.")
                return False

                
        opponent_pieces = self.get_opponent_pieces()
        valid_move = False
        opponent_pieces_encountered = 0
        row_direction = 1 if end_row > start_row else -1
        col_direction = 1 if end_col > start_col else -1
        
        current_row, current_col = start_row, start_col
        while current_row != end_row or current_col != end_col:
            current_row += row_direction
            current_col += col_direction
            
            if not (1 <= current_row < 9 and 1 <= current_col < 9):
                break
            #Statment for invalid move when it's not empty square or an opponent piece.
            piece_at_current_square = self.board[current_row][current_col]
            if piece_at_current_square in opponent_pieces:
                opponent_pieces_encountered += 1
            elif piece_at_current_square != "[_]":
                return False

        #Determine piece king.
        is_king = start_piece in ["[KO]", "[KX]"]
        step_row = int(row_diff / abs(row_diff))
        step_col = int(col_diff / abs(col_diff))

        #IF statment to check each square along the path for kings
        if is_king:
            current_row, current_col = start_row, start_col
            while (current_row, current_col) != (end_row - step_row, end_col - step_col):
                current_row += step_row
                current_col += step_col
                if self.board[current_row][current_col] != "[_]":
                    print("Path is blocked.")
                    return False
        

        return True

        #Assuming any move that doesn't capture more than one piece and ends on an empty square is valid.
        if opponent_pieces_encountered <= 1 and self.board[end_row][end_col] == "[_]":
            valid_move = True

        #Non-capture move validation for normal pieces and kings.
        row_diff = end_row - start_row
        col_diff = abs(end_col - start_col)  #Absolute value for column difference since move can be left or right.
        
        #Normal piece move validation.
        if piece in ["[O]", "[KO]"] and self.current_player == "[O]" and row_diff == 1 and col_diff == 1:
            return True
        elif piece in ["[X]", "[KX]"] and self.current_player == "[X]" and row_diff == -1 and col_diff == 1:
            return True
        else:
            print("Invalid move. Normal pieces can only move forward diagonally by one square.")
            return False

        #King move validation.
        if piece == "[KO]" or "[KX]" and abs(row_diff) == 1 and col_diff == 1:
            valid_move = True

        return valid_move

    def make_move(self, start_row, start_col, end_row, end_col):        
        piece = self.board[start_row][start_col]
        self.board[start_row][start_col] = "[_]"  
        self.board[end_row][end_col] = piece  
        #Check piece for promotion.
        self.promote_to_king(end_row, end_col)

        #Checking for capture move by comparing row positions directly and remove the captued piece.
        if abs(start_row - end_row) == 2 and abs(start_col - end_col) == 2:
            intermediate_row = (start_row + end_row) // 2
            intermediate_col = (start_col + end_col) // 2
            self.board[intermediate_row][intermediate_col] = "[_]"
        
        additional_captures = self.find_possible_captures(end_row, end_col)
        

    def is_capture_move(self, start_row, start_col, end_row, end_col):
        #Check if the move is a diagonal jump over an opponent piece
        if abs(end_row - start_row) == 2 and abs(end_col - start_col) == 2:
            mid_row = (start_row + end_row) // 2
            mid_col = (start_col + end_col) // 2
            if self.board[mid_row][mid_col] in self.get_opponent_pieces() and self.board[end_row][end_col] == "[_]":
                return True, mid_row, mid_col  #Return also the position of the captured piece.
        return False, None, None

    
    def can_move(self, row, col):
    #Determine piece type and direction based on the current player.
        piece = self.board[row][col]
        directions = [(1, -1), (1, 1)]  
        if piece in ["[KO]", "[KX]"]:  
            directions += [(-1, -1), (-1, 1)]  

        for drow, dcol in directions:
            new_row, new_col = row + drow, col + dcol
            if 1 <= new_row <= 8 and 1 <= new_col <= 8: #Check if new position is within the board.
                if self.board[new_row][new_col] == "[_]": #Check if the destination square is empty.
                    return True
        return False

    def get_opponent_pieces(self): #Determine opponent pieces.
        if self.current_player in ["[O]", "[KO]"]:
            return ["[X]", "[KX]"]
        else:
            return ["[O]", "[KO]"]

    def can_capture(self, row, col):
        #Determine piece type and direction based on the current player.
        piece = self.board[row][col]
        opponent_pieces = self.get_opponent_pieces()
        capture_directions = [(2, -2), (2, 2)]  #Directions: Down-left, Down-right for "O", adjust for "X"
        if piece in ["[KO]", "[KX]"]:  #If the piece is a king, it need to capture up 
            capture_directions += [(-2, -2), (-2, 2)]  # Directions: Up-left, Up-right

        for drow, dcol in capture_directions:
            mid_row, mid_col = row + drow // 2, col + dcol // 2
            new_row, new_col = row + drow, col + dcol
            
            if 1 <= new_row <= 8 and 1 <= new_col <= 8 and 1 <= mid_row <= 8 and 1 <= mid_col <= 8: #Checking if new and mid positions are within the board.
                if self.board[mid_row][mid_col] in opponent_pieces and self.board[new_row][new_col] == "[_]": #Checking if the mid square has an opponent piece and the destination square is empty.
                    return True
        return False

    def has_legal_moves(self): #checking for legal move.
        for row in range(1, 9):  
            for col in range(1, 9):
                if self.is_correct_players_piece(row, col) and self.can_move(row, col) or self.can_capture(row, col):
                    return True
        return False

    def check_for_draw(self):
        for row in range(9):
            for col in range(9):
                if self.is_correct_players_piece(row, col):
                    if self.has_legal_moves():
                        return False  
        return True  
    
    def play(self):
        self.checkers_conditions()  #Game rules.
        self.create_pieces()  #Set up  pieces.

        game_over = False
        while not game_over:  #Starting main game loop.
            self.refresh_board()  #Showing the current state of the board.
            move_made = False

            while not move_made:  #Loop until a valid move is made.
                start_row, start_col, end_row, end_col = self.taking_move()  #Geting player's move.

                #Execute the move, handle capture, and promote to king if applicable.
                if self.is_move_valid(start_row, start_col, end_row, end_col, self.current_player):
                    self.make_move(start_row, start_col, end_row, end_col)
                    move_made = True  #Statment for valid move.

                    #Check for additional captures after a capture.
                    is_capture, captured_row, captured_col = self.is_capture_move(start_row, start_col, end_row, end_col)
                    while is_capture:
                        self.board[captured_row][captured_col] = "[_]"  
                        additional_captures = self.find_possible_captures(end_row, end_col)
                        if not additional_captures:
                            print("No captures avaliable for now.")
                            break
                        else:
                            print("Additional captures possible. You must continue capturing.")
                            start_row, start_col = end_row, end_col  
                            end_row, end_col = self.taking_move()  
                            is_capture, captured_row, captured_col = self.is_capture_move(start_row, start_col, end_row, end_col)

                        if is_capture:
                            self.make_move(start_row, start_col, end_row, end_col)
                            self.board[captured_row][captured_col] = "[_]"  
                        else:
                            print("Invalid capture move, try again.") 
                            
                    

            #Checking for a winne.
            winner = self.check_winner()
            if winner:
                print(f"Game Over. {winner} wins!")
                break  

            #Check for draw as an option.
            if self.check_for_draw():
                print("Game Over. It's a draw!")
                break

            self.switch_player()  #Switch players for the next turn.

#Function that start to use the class:
if __name__ == "__main__":
    game = CheckersGame()
    game.play()


