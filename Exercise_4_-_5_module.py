# Below you can see my answers for exercises from 4 - 5 chapter in "Python Basics". 


"""
Chapter 4.1
"""
#1. Print a string that uses double quotation marks inside the string.

print('She whisper to me really slowly; "I think you are nice." ') 

#2 Print a string that uses an apostrophe inside the string.

print("This isn't a new thing for me! But thanks!" )

#3 Print a string that spans multiple lines, with whitespace preserved.

print("""This is could be a fragment from a book that I would to write someday.
      A book with a soul, trully and entirely speaking about needs of young people in nowadays.
          With one special character and his adventures beayond divisions. With love and enjoy,  in the end. """)
             
#4 Print a string that is coded on multiple l

print("This is a long sentence that I need to write to make the exercise\
correct answer. To achive that I need to focus on what I am writing, unless\
no one is gonna to read this, so I'm doing this to make this long sentence.\
I'm trully sorry for you to read this, regards to see the last word!")


"""
Chapter 4.2
"""
#1. Create a string and print its length using the len() function.

lenghtCheckString = "Correct"
print(len(lenghtCheckString))

#2. Create two strings, concatenate them, and print the resulting string.

stringOne = "John"
stringTwo = "Smith"
stringThree = stringOne +  stringTwo
print(stringThree)

#3. Create two strings and use concatenation to add a space inbetween them. Then print the result.

string1 = "Bob"
string2 = "Smiley"
string3 = string1 + " " + string2
print(string3)

#4. Print the string "zing" by using slice notation on the string "bazinga" to specify the correct range of characters.

exerciseWord = "bazinga"
print(exerciseWord[2:6])


"""
Chapter 4.3
"""
#1. Write a script that converts the following strings to lowercase: "Animals", "Badger", "Honey Bee", "Honeybadger".
#Print each lowercase string on a separate line.

string1 = "Animals"
string2 = "Badger"
string3 = "Honey Bee"
string4 = "Honeybadger"

print(string1.lower())
print(string2.lower())
print(string3.lower())
print(string4.lower())

#2. Repeat Exercise 1, but convert each string to uppercase instead of lowercase.
print(string1.upper())
print(string2.upper())
print(string3.upper())
print(string4.upper())

#3. Write a script that removes whitespace from the following strings:
#string1 = " Filet Mignon"
#string2 = "Brisket "
#string3 = " Cheeseburger "
#Print out the strings with the whitespace removed.

string1 = " Filet Mignon"
string2 = "Brisket "
string3 = " Cheeseburger "

string1 = string1.lstrip()
string2 = string2.rstrip()
string3 = string3.strip()

print(string1)
print(string2)
print(string3)


#4. Write a script that prints out the result of .startswith("be") on each of the following strings:
#string1 = "Becomes"
#string2 = "becomes"
#string3 = "BEAR"
#string4 = " bEautiful"

string1 = "Becomes"
string2 = "becomes"
string3 = "BEAR"
string4 = " bEautiful"

print(string1.startswith("be"))
print(string2.startswith("be"))
print(string3.startswith("be"))
print(string4.startswith("be"))

#5. Using the same four strings from Exercise 4,
#write a script that uses string methods to alter each string so that .startswith("be") returns True for all of them.

string1 = "Becomes"
string2 = "becomes"
string3 = "BEAR"
string4 = " bEautiful"

string1 = string1.lower()
string3 = string3.lower()
string4 = string4.lstrip().lower()

print(string1.startswith("be"))
print(string2.startswith("be"))
print(string3.startswith("be"))
print(string4.startswith("be"))


"""
Chapter 4.4
"""
#1. Write a script that takes input from the user and displays that input back.

word = input("What day is today? ")
print("You have said that today is " + word)

#2. Write a script that takes input from the user and displays the input in lowercase.

word1 = input("Can you say something loud? ")
print("I will whisper what you said: " + word1.lower())

#3. Write a script that takes input from the user and displays the number of characters inputted.

word2 = input("Say me a word and I will give you back number of characters it contains! ")
print(f"It contains {len(word2)} characters!")


"""
Chapter 4.5
"""
#Write a script named first_letter.py that first prompts the user for input by using the string "Tell me your password:"
#The script should then determine the first letter of the user’s input, convert that letter to upper-case, and display it back.
#For example, if the user input is "no" then the program should respond like this:
#The first letter you entered was: N
#For now, it’s okay if your program crashes when the user enters nothing as input—that is, they just hit Enter instead of typing something in.
#You’ll learn about a couple of ways you can deal with this situation in an upcoming chapter.

first_letter = input("Tell me your password: ")
first_letter = first_letter[0].upper()
print("The first letter you entered was: " + first_letter)


"""
Chapter 4.6
"""
#1. Create a string containing an integer, then convert that string into an actual integer object using int(). Test that your new object is
#a number by multiplying it by another number and displaying the result.

string = "3"
string = int(string)

print(string * 3)

#2. Repeat the previous exercise, but use a floating-point number and float().

string = "3"
string = float(string)

print(string * 3)

#3. Create a string object and an integer object, then display them sideby-side with a single print statement by using the str() function.

string = "3"
integer = 3

print(string + str(integer))

#4. Write a script that gets two numbers from the user using the input() function twice, multiplies the numbers together, and 
#displays the result. If the user enters 2 and 4, your program should print the following text: The product of 2 and 4 is 8.0.

number1 = input("Write first number: ")
number2 = input("Write second number: ")
number1 = int(number1)
number2 = int(number2)

print(f"The product of {number1} and {number2} is {float(number1 * number2)}")


"""
Chapter 4.7
"""
#1. Create a float object named weight with the value 0.2, and create a string object named animal with the value "newt". Then use these
#objects to print the following string using only string concatenation: 0.2 kg is the weight of the newt.

weight = 0.2
animal = "newt"

print(str(weight) + " kg is the weight of the " + animal)

#2. Display the same string by using the .format() method and empty {} place-holders.

weight = 0.2
animal = "newt"

print("{} kg is the weight of the {}".format(weight, animal))

#3. Display the same string using an f-string

print(f"{weight} kg is the weight of the {animal}")


"""
Chapter 4.8
"""
#1. In one line of code, display the result of trying to .find() the substring "a" in the string "AAA". The result should be -1.

string = "AAA"
print(string.find("a"))

#2. Replace every occurrence of the character "s" with "x" in the string "Somebody said something to Samantha.".

string = "Somebody said something to Samantha."
string = string.replace("s", "x")

print(string)

#3. Write and test a script that accepts user input using the input() function and displays the result of trying to .find() a particular
#letter in that input.

word = input("Write a word with 'a'. I will try to find 'a' letter in it ")

print(f"The a is is under {word.find('a')}")

"""
4.9
"""

#Write a script called translate.py that asks the user for some input with the following prompt: Enter some text:. Then use the .replace()
#method to convert the text entered by the user into “leetspeak” by making the following changes to lower-case letters:
#• The letter a becomes 4
#• The letter b becomes 8
#• The letter e becomes 3
#• The letter l becomes 1
#• The letter o becomes 0
#• The letter s becomes 5
#• The letter t becomes 7
#Your program should then display the resulting string as output. Below is a sample run of the program:
#Enter some text: I like to eat eggs and spam.
#I 1ik3 70 347 3gg5 4nd 5p4m.

text = input("Enter some text: ")

text = text.replace("a", "4")
text = text.replace("b", "8")
text = text.replace("e", "3")
text = text.replace("l", "1")
text = text.replace("o", "0")
text = text.replace("s", "5")
text = text.replace("t", "7")

print(text)

"""
5.1 Review Exercises
"""
#1. Write a script that creates the two variables, num1 and num2. Both num1 and num2 should be assigned the integer literal 25,000,000,
#one written with underscored and one without. Print num1 and num2 on two separate lines.

num1 = 25_000_000
num2 = 25000000

print(num1)
print(num2)

#2. Write a script that assigns the floating-point literal 175000.0 to the variable num using exponential notation,
#and then prints num in the interactive window.

num = 175e3
print(num)

#3. In IDLE’s interactive window, try and find the smallest exponent N so that 2e<N>, where <N> is replaced with your number, returns inf

print(2e308)

"""
Chapter 5.3
"""
#Challenge: Perform Calculations on User Input
#Write a script called exponent.py that receives two numbers from the user
#and displays the first number raised to the power of the second number.
#A sample run of the program should look like this (with example input that has been provided by the user included below):
#Enter a base: 1.2
#Enter an exponent: 3
#1.2 to the power of 3 = 1.7279999999999998
#Keep the following in mind:
#1. Before you can do anything with the user’s input, you will have to assign both calls to input() to new variables.
#2. The input() function returns a string, so you’ll need to convert the user’s input into numbers in order to do arithmetic.
#3. You can use an f-string to print the result.
#4. You can assume that the user will enter actual numbers as input.

base = input("Enter a base: ")
exponent = input("Enter an exponent: ")

base = float(base)
exponent = int(exponent)

print(f"{base} to the power of {exponent} = {base ** exponent}")
      
"""
Chapter 5.5
"""

#1. Write a script that asks the user to input a number and then displays that number rounded to two decimal places. When run, your
#program should look like this:
#Enter a number: 5.432
#5.432 rounded to 2 decimal places is 5.43

number = float(input("Please, write a number "))
print(f"{round(number, 2)}")

#2. Write a script that asks the user to input a number and then displays the absolute value of that number. When run, your program
#should look like this:
#Enter a number: -10
#The absolute value of -10 is 10.0

number = int(input("Enter a number: "))
print(f"The absolute valute of {number} is {abs(number)}")

#3. Write a script that asks the user to input two numbers by using the
#input() function twice, then display whether or not the difference
#between those two number is an integer. When run, your program

num1 = float(input("Enter a number: "))
num2 = float(input("Enter another number: "))

print(f"The difference between {num1} and {num2} is an integer? {(num1 - num2).is_integer()}")


"""
Chapter 5.6
"""

#1. Print the result of the calculation 3 ** .125 as a fixed-point number with three decimal places.

exercise = 3 ** .125

print(round(exercise, 3))

#2. Print the number 150000 as currency, with the thousands grouped with commas.
#Currency should be displayed with two decimal places.

number = 150000

print(f"{number:,.2f}")

#3. Print the result of 2 / 10 as a percentage with no decimal places.
#The output should look like 20%.
result = 2 / 10
print(f"{result:.0%}")
