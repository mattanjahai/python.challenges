# Below you have a full code of my HangMan Game, have fun!

import random

print("Welcome to the game of Hangman! Your task is to guess the mysterious word, playing the role of a detective trying to uncover the hidden word. In this exciting game,\
you will have the opportunity to demonstrate your word and logic skills. However, remember that each unsuccessful attempt brings the hangman one step closer to his inevitable fate. Have a nice time!")

number_of_tries = 10

words = ["apple", "orange", "strawberry", "raspberry", "gooseberry", "plum", "grape", "mango", "kiwi", "pineapple", "peach", "damson", "cherry", "blueberry", "chokeberry", "pomegranate", "kiwi", "starfruit"]

game_word = random.choice(words)

guessed_letters = []

x = 9

hang = [""" _______
|       |
|       O
|      /|\\
|      / \\
|
|_________""",
""" _______
|       |
|       O
|      /|\\
|      /
|
|_________""",
""" _______
|       |
|       O
|      /|\\
|      
|
|_________""",
""" _______
|       |
|       O
|      /|
|      
|
|_________""",
""" _______
|       |
|       O
|       |
|      
|
|_________""",
""" _______
|       |
|       O
|      
|      
|
|_________""",
""" _______
|       |
|       
|      
|      
|
|_________""",
""" _______
|       
|       
|      
|      
|
|_________""",
"""
|       
|       
|      
|      
|
|_________""",
"""






_________"""

]

# Below engine of the game:

while number_of_tries > 0:
               
    word = ""
    
    for letter in game_word:
        if letter in guessed_letters:
            word = word + letter
        else:
            word = word + "_"
    print(word)

    if "_" not in word:
        print(f"Congratulations! The word was '{game_word.upper()}'! You have won!")
        break
    
    choose_letter = input(f"You have {number_of_tries} left. Quess a letter!: ").lower()

    if choose_letter in guessed_letters:
        print("You have allready choosed this letter, try again!")
        continue

    if len(choose_letter) > 1:
        print("You have type wrong the letter, try again!")
        continue
    
    guessed_letters.append(choose_letter)

    if choose_letter in game_word:
        print("Nice guess! We have it!")
    else:
        print("Sorry! There is no letter like that in this word!")
        print(hang[x])
        x -= 1
        number_of_tries = number_of_tries - 1  
    
if number_of_tries == 0:
    print("Sorry, you lose! Try next time!")
        
