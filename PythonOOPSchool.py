class School:
    def __init__(self, name, city):
        self.name = name
        self.city = city

    def __str__(self):
        return f"We are {self.name} school located in {self.city} town."


class Building:
    def __init__(self, name, address_street, address_number):
        self.name = name
        self.street = address_street
        self.number = address_number

    def __str__(self):
        return f"{self.name} building is located at {self.number}, {self.street}"


class Administration(Building):
    def __str__(self):
        return super().__str__() + "\nWelcome to the Administration building, for more help ask an employee under room 3."


class Gym(Building):
    def __init__(self, name, address_street, address_number):
        super().__init__(name, address_street, address_number)
        self.gym_capacity = 40
        self.permanent_tickets = 0

    def num_of_tickets(self):
        tickets = self.gym_capacity - self.permanent_tickets
        return tickets

    def stop_selling_tickets(self, tickets):
        if tickets + self.permanent_tickets >= self.gym_capacity:
            return "We have sold all the tickets for this month, please ask administration for more information."
        else:
            return f"We have {tickets} permanent tickets available for now."


class Classroom(Building):
    def __init__(self, name, address_street, address_number, min_students, max_students):
        super().__init__(name, address_street, address_number)
        self.min_students = min_students
        self.max_students = max_students
        self.students = []

    def __str__(self):
        return super().__str() + f"\nThis classroom is for {self.name}. Lesson classes start from {self.min_students} and the list ends with {self.max_students} registered students."

    def add_student(self, st):
        if len(self.students) < self.max_students:
            self.students.append(st)
            st.location = self.name
            return True
        else:
            print("Maximum number of students reached.")
            return False

    def remove_student(self, st):
        if st in self.students:
            self.students.remove(st)
            st.location = ""
            return True
        else:
            print("Student not found.")
            return False


class Chemistry(Classroom):
    def __str__(self):
        return super().__str() + f"\nPrepare a white coat and safety goggles!\nFor more information, contact the Administration. Currently in {self.name}."


class Biology(Classroom):
    def __str__(self):
        return super().__str() + f"\nSamples of fungi, plants, and animals will be needed!\nFor more information, contact the Administration. Currently in {self.name}."


class Mathematics(Classroom):
    def __str__(self):
        return super().__str() + f"\nPrepare a protractor, ruler, set square, and calculator!\nFor more information, contact the Administration. Currently in {self.name}."


class Dormitory(Building):
    def __init__(self, name, address_street, address_number, n_rooms, min_students=1, max_students=50):
        super().__init__(name, address_street, address_number)
        self.min_students = min_students
        self.max_students = max_students
        self.n_rooms = n_rooms
        self.rooms = {}  
        self.available_rooms = n_rooms

    def __str__(self):
        return super().__str() + f"\nDormitory {self.name} is hosting {len(self.rooms)} students."

    def student_check_in(self, st):
        if self.available_rooms > 0:
            st.location = self.name
            self.available_rooms -= 1
            room_assigned = False
            for room_number in range(1, self.n_rooms + 1):
                if room_number not in self.rooms:
                    self.rooms[room_number] = st
                    room_assigned = True
                    return True
            if not room_assigned:
                print("Unfortunately, no free rooms available. Please contact the Administration.")
                return False

    def student_check_out(self, st):
        for room_number, student in self.rooms.items():
            if student == st:
                del self.rooms[room_number]
                self.available_rooms += 1
                st.location = ""
                print(f"Student {st.name} checked out successfully.")
                return True
        print("Student not found.")
        return False


class Person:
    
    age_until_retirement_m = 65
    age_until_retirement_k = 60

    def __init__(self, name, age, gender, location):
        self.name = name
        self.age = age
        self.gender = gender
        self.location = location
        Person.add_employee()
        Person.add_student()

    @classmethod
    def number_of_employees(cls):
        return cls.number_of_employee

    @classmethod
    def add_employee(cls):
        cls.number_of_employee += 1

    @classmethod
    def add_student(cls):
        cls.number_of_students += 1

    def __str__(self):
        return f"{self.name} - currently in {self.location}"


class Employee(Person):
    number_of_employee = 0
    
    def __init__(self, name, age, gender, location):
        super().__init__(name, age, gender, location)

    def years_until_retirement(self):
        if self.gender == 'm':
            retirement_age = Person.age_until_retirement_m
        else:
            retirement_age = Person.age_until_retirement_k

        years_left = retirement_age - self.age
        return years_left


class Teacher(Employee):
    def __init__(self, name, age, gender, location, subject):
        super().__init__(name, age, gender, location)
        self.subject = subject

    def __str__(self):
        return super().__str() + f"\nIs a {self.subject} teacher."


class Student(Person):
    number_of_students = 0

    def __init__(self, name, age, gender, location, school_yearbook):
        super().__init__(name, age, gender, location)
        self.school_yearbook = school_yearbook

    def __str__(self):
        return super().__str() + f"\n{self.name} is a member of the school in {self.school_yearbook} year."
